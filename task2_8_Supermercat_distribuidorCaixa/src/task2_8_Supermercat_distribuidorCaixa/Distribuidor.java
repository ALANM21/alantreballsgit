package task2_8_Supermercat_distribuidorCaixa;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Distribuidor {

	private ArrayList<Caixa> caixes = new ArrayList<Caixa>();
	
	private Lock bloqueig;
	private boolean caixaLliure;
	private Condition clientCondicio;

	public Distribuidor(int numCaixes) {
		super();
		bloqueig = new ReentrantLock();
		clientCondicio = bloqueig.newCondition();
		caixaLliure = true;

		for (int i = 0; i < numCaixes; i++) {
			caixes.add(new Caixa("Caixa" + (i + 1)));
		}
	}

	public Caixa assignaCaixa() {
		bloqueig.lock();
		Caixa caixaDisponible = null;
		try {

			if (!caixaLliure) {
				System.out.println("<DISTRIBUIDOR> Cap caixa esta disponible, " + "dorm ["
						+ Thread.currentThread().getName() + "] zzzz...");
				clientCondicio.await();
			}
			
			for (Caixa caixa : caixes) {
				if (caixa.isAvailable()) {
					caixaDisponible = caixa;
					caixa.setIsAvailable(false);
					break;
				}
			}
			caixaLliure = caixes.stream().anyMatch(c -> c.isAvailable());
			
			for (int i = 0; i < 1000000; i++) {}
			System.out.println("<DISTRIBUIDOR> Assigna [" + Thread.currentThread().getName() + "] a <"
					+ caixaDisponible.getName() + ">");
			
			bloqueig.unlock();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return caixaDisponible;
	}
	
	public void finalitzaCompra() {
		bloqueig.lock();
		clientCondicio.signal();
		bloqueig.unlock();
		
	}

}