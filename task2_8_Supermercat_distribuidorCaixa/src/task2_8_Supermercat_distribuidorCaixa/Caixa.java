package task2_8_Supermercat_distribuidorCaixa;


public class Caixa {

		//Atributs
		private String name;
		private boolean estaDisponible;
		
		//Constructor
		public Caixa() {
			estaDisponible = true;
		}
		
		public Caixa(String name) {
			estaDisponible = true;
			this.name = name;
			System.out.println("Oberta la caixa <"+name+">");
		}	

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isAvailable() {
			return estaDisponible;
		}
		
		public void setIsAvailable(boolean isAv) {
			this.estaDisponible = isAv;
		}
		
		public void agafaCompra() {
			for (int i = 0; i < 10000; i++) {
				
			}
			System.out.println("<"+ name + "> Atenguent a " + Thread.currentThread().getName());
		}		
		
		public void finalitzaCompra() {
			System.out.println("<"+ name + "> Finalitzada atencio a " + Thread.currentThread().getName());
			this.estaDisponible = true;
		}
						

}