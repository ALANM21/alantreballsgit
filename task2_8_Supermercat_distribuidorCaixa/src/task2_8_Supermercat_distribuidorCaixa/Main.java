package task2_8_Supermercat_distribuidorCaixa;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("[main] Programa del supermercat - Distribuidor");
		System.out.println("==============================================");

		ArrayList<Thread> fils = new ArrayList<Thread>();

		Distribuidor distributor = new Distribuidor(3); // Distribuidor amb numero de caixes assignades
		
		// Creem 5 clients
		for (int i=1 ; i < 6 ; i++) {
			fils.add(new Thread(new Client(distributor),"Client"+i));
			
		}
		
		// Arranquem els fils
		for (Thread fil : fils) {
			fil.start();
		}
		
		
		fils.stream().forEach(fil -> {
			try {
				fil.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		System.out.println("[main] Finalitzacio programa del supermercat");


	}

}