package task2_8_Supermercat_distribuidorCaixa;

import java.text.DecimalFormat;

public class Client implements Runnable{
	
	private Distribuidor distributor;
	private Caixa caixa;
	String nomFil="";

	public Client(Distribuidor distributor) {
		super();
		this.distributor = distributor;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}

	@Override
	public void run() { //S'executa el codi dels fils
		
		Float numeroRandom = 30f + (float) (Math.random() * ( 45 - 30 ));
		DecimalFormat formatPreu = new DecimalFormat("#.00");
		String preu = formatPreu.format(numeroRandom);
		
		
		nomFil = Thread.currentThread().getName();
		
			clientGoToQueue();

			this.caixa = distributor.assignaCaixa();
			caixa.agafaCompra();
			clientPay(preu);
			caixa.finalitzaCompra();
			distributor.finalitzaCompra();
			clientGoOut();		
	}
	
	public void clientGoToQueue() {
		System.out.println("["+nomFil+"] Client va a la cola del distribuidor");
	}
	
	public void clientPay(String preu) {
		System.out.println("["+nomFil+"] Ha fet la compra de productes amb un preu de " + preu +" €");
	}
	
	public void clientGoOut() {
		System.out.println("[" + nomFil+ "] Client ix del centre comercial");

	}
	
	
	

	
}
