package task_2_3_Ford;

public class Components {

	//Atributos
	private String carroceria;
	private String motor;
	private String rodes;

	//Constructor
	public Components(String carroceria, String motor, String rodes) {
		super();
		this.carroceria = carroceria;
		this.motor = motor;
		this.rodes = rodes;
	}
	
	//Getters y setters
	public String getCarroceria() {
		return carroceria;
	}

	public void setCarroceria(String carroceria) {
		this.carroceria = carroceria;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getRodes() {
		return rodes;
	}

	public void setRodes(String rodes) {
		this.rodes = rodes;
	}

	//Encarregar pieza
	public void encarregantPieza(String pieza) {
		System.out.println("Encarregant una pieza de " + pieza);
	}

	
		
}
