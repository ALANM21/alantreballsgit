package task_2_3_Ford;

import  task_2_3_Ford.*;

public class Main extends Thread {

	//Main
	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("PROGRAMA MUNTATGE COTXES");
		System.out.println("========================");
		
		//Creem un nou component
		Components components = new Components("carroceria", "motor", "rodes");
		
		//A la linea de muntatje li asignem un component
		LineaMuntatge linea = new LineaMuntatge(components);
		
		//Creem el fil
		Thread filA = new Thread(linea, "lineaA");
		Thread filB = new Thread(linea, "lineaB");
		Thread filC = new Thread(linea, "lineaC");

		filA.start(); //Inciem
		filB.start(); //Inciem
		filC.start(); //Inciem
		
		filA.join(); //Espera a que s'execute el fil
		filB.join(); //Espera a que s'execute el fil
		filC.join(); //Espera a que s'execute el fil
		
	}//Main
	
}//Class
