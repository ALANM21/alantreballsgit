package task_2_3_Ford;

public class LineaMuntatge implements Runnable{

	private Components components;

	//CONSTRUCTOR
	public LineaMuntatge(Components components) {
		super();
		this.components = components;
	}

	//GETTERS I SETTERS
	public Components getComponents() {
		return components;
	}

	public void setComponents(Components components) {
		this.components = components;
	}
	
	
	//METODOS
	public void cercaComponentCarroceria(Components components) {
		System.out.println("Cerca component " + components.getCarroceria() + " ..");
	}
	
	public void cercaComponentMotor(Components components) {
		System.out.println("Cerca component " + components.getMotor() + " ..");
	}
	
	public void cercaComponentRodes(Components components) {
		System.out.println("Cerca component " + components.getRodes() + " ..");
	}
	
	
	//RUN
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}


	
	
	
	
}