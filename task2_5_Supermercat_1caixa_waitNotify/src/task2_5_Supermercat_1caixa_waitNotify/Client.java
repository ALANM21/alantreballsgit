package task2_5_Supermercat_1caixa_waitNotify;

public class Client extends Thread {

	private Caixa caixa;
	private String nomCaixa;
	
	//Constructor
	public Client(String nomCaixa, Caixa caixa) {
		super();
		this.caixa = caixa;
		this.nomCaixa = nomCaixa;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();

		System.out.println("[" + Thread.currentThread().getName() + "]"  + "El client ha fet la recollida"
				+ " de productes");

		System.out.println("[" + Thread.currentThread().getName() + "]"  + "Client va a la caixa <Caixa1>");
		
			caixa.metodeWait(); //Esperem a que estiga la caixa disponible
			
			caixa.agafaCompra();
			caixa.cobramentCompra();
			caixa.ticketCompra();
			
			caixa.metodeCanviaEstatCaixa();
		
	}
	
	
}
