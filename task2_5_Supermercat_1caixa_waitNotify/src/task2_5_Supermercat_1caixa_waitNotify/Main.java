package task2_5_Supermercat_1caixa_waitNotify;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("[main] Programa del supermercat amb 1 caixa - waitNotify");
		System.out.println("========================================================");
		System.out.println("Oberta la caixa <Caixa1>");
		
		Caixa caixa1 = new Caixa("caixa1");
		Client client1 = new Client("client1", caixa1);
		Client client2 = new Client("client2", caixa1);
		Client client3 = new Client("client3", caixa1);
		
		Thread clientThread1 = new Thread(client1, "client1");
		Thread clientThread2 = new Thread(client2, "client2");
		Thread clientThread3 = new Thread(client3, "client3");
		
		clientThread1.start();
		clientThread2.start();
		clientThread3.start();
		
		//Espera fils
		try {
			clientThread1.join();
			clientThread2.join();
			clientThread3.join();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println("[main] Finalitzacio programa del supermercat");


	}

}
