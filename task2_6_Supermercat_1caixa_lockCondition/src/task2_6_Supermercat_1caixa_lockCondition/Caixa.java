package task2_6_Supermercat_1caixa_lockCondition;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Caixa {

	//Atributs
	private boolean caixaDisponible;
	private String nomCaixa;
	private Lock cadena;
	private Condition disponible;
	
	//Constructor
	public Caixa(String nomCaixa) {
		super();
		this.caixaDisponible = true;
		this.nomCaixa = nomCaixa;
		
		cadena = new ReentrantLock();
		disponible = cadena.newCondition();
	}

	//Saber si la caixa esta disponible ---
	public boolean isCaixaDisponible() {
		if (caixaDisponible) {
			return true;
		} else {
			return false;
		}
	}

	//Agafar la compra -------------------
	public void agafaCompra() {
		//Fent temps per modificar variable booleana
		for (int i = 0; i < 10000; i++) {
			//No fer res
		}

		this.caixaDisponible = false; //Canviant estat de la caixa
		System.out.println("<Caixa1> Llegint la compra de" 
				+ "[" + Thread.currentThread().getName() + "]");
	}

	public void cobramentCompra() {
				
		System.out.println("<Caixa1> Import de la compra del client " 
				+ "[" + Thread.currentThread().getName() + "] es de " + Math.round(Math.random() * 250) + " € ..");
	}

	public void ticketCompra() {
		
		System.out.println("<Caixa1> Donant ticket compra a " 
				+ "[" + Thread.currentThread().getName() + "]");
		this.caixaDisponible = true;
	}
	
	//-------------------------------------
	public void metodeWait() throws InterruptedException {
		try {
			cadena.lock();
			while(!isCaixaDisponible()) {
				System.out.println("["+Thread.currentThread().getName()+"] metodeWait -> "
						+ "La caixa no esta disponible");
				disponible.await();
			}
			
			agafaCompra();
			cadena.unlock();
			
		} catch (InterruptedException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	//-------------------------------------
	public void metodeCanviaEstatCaixa() throws InterruptedException {
		cadena.lock();
		ticketCompra();
		
		System.out.println("["+Thread.currentThread().getName()+"] metodeCanviaEstatCaixa -> "
				+ "La caixa ja esta disponible");
		disponible.signalAll();
		cadena.unlock();
	}
	
}
