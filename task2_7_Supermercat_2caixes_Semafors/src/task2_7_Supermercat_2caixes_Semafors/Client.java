package task2_7_Supermercat_2caixes_Semafors;

public class Client extends Thread {

	private String nomCaixa;
	private Caixa caixa;
	
	//Constructor
	public Client(String nomCaixa, Caixa caixa1, Caixa caixa2) {
		super();
		this.nomCaixa = nomCaixa;
		this.caixa = caixaAleatoria(caixa1, caixa2);
	}

	public Caixa caixaAleatoria(Caixa caixa1, Caixa caixa2) {
		long numeroAleatorio = Math.round(Math.random() * 1);
		if (numeroAleatorio == 0) {
			return caixa1;
		} else {
			return caixa2;
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();

		System.out.println("[" + Thread.currentThread().getName() + "]"  + "El client ha fet la recollida"
				+ " de productes");

		System.out.println("[" + Thread.currentThread().getName() + "]"  + "Client va a la caixa <Caixa1>");
		
				
		caixa.agafaCompra();
		caixa.cobramentCompra();
		caixa.ticketCompra();
			
		caixa.metodeCanviaEstatCaixa();
	}
	
	
}
