package task2_7_Supermercat_2caixes_Semafors;

public class SemaforComptador {

	//Atribut
	private int numRecursosDisponibles;
	
	//Constructor
	public SemaforComptador(int numAccessos) {
		this.numRecursosDisponibles = numAccessos;
		System.out.println("["+ Thread.currentThread().getName() +
				"] Creat el SEMAFOR COMPTADOR amb " + numAccessos + " permesos.");
	}
	
	//----------------
	public synchronized void metodeWait() {
		while(numRecursosDisponibles == 0) { //Mentre no condicio, bloquejat el fil
			try {
				System.out.println("["+ Thread.currentThread().getName() +
						"] SEMAFOR bloqueja execució -> Espera zZzZzZ...");
				wait();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}//while
		numRecursosDisponibles--; //Canvia condicio, bloquejat fil executar
	}
	
	
	//------------------
	public synchronized void metodeWakeUp() {
		numRecursosDisponibles++;
		System.out.println("["+ Thread.currentThread().getName() +
				"] SEMAFOR desperta execució -> Desperta fils...");
		notify();
	}
}
