package task2_7_Supermercat_2caixes_Semafors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("[main] Programa del supermercat amb 1 caixa - Semafors");
		System.out.println("======================================================");
		System.out.println("Oberta la caixa <Caixa1> i <Caixa2>");
		
		//Creem el semafor compartit
		SemaforComptador semaforCompartit = new SemaforComptador(2);
		
		Caixa caixa1 = new Caixa("caixa1", semaforCompartit);
		Caixa caixa2 = new Caixa("caixa2", semaforCompartit);

		
		Client client1 = new Client("client1", caixa1, caixa2);
		Client client2 = new Client("client2", caixa1, caixa2);
		Client client3 = new Client("client3", caixa1, caixa2);
		Client client4 = new Client("client3", caixa1, caixa2);
		
		Thread clientThread1 = new Thread(client1, "client1");
		Thread clientThread2 = new Thread(client2, "client2");
		Thread clientThread3 = new Thread(client3, "client3");
		Thread clientThread4 = new Thread(client3, "client4");

		clientThread1.start();
		clientThread2.start();
		clientThread3.start();
		clientThread4.start();

		//Espera fils
		try {
			clientThread1.join();
			clientThread2.join();
			clientThread3.join();
			clientThread4.join();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println("[main] Finalitzacio programa del supermercat");


	}

}
