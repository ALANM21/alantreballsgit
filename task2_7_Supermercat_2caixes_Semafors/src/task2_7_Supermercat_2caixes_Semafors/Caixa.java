package task2_7_Supermercat_2caixes_Semafors;

import java.util.Random;

public class Caixa {

	//Atributs
	private boolean caixaDisponible;
	private String nomCaixa;
	private final SemaforComptador semafor;
	
	//Constructor
	public Caixa(String nomCaixa, SemaforComptador semafor) {
		super();
		this.caixaDisponible = true;
		this.nomCaixa = nomCaixa;
		this.semafor = semafor;
	}

	public boolean cercaCaixa() {
		semafor.metodeWait();
		return caixaDisponible;
	}
	
	//Saber si la caixa esta disponible ---
	public boolean isCaixaDisponible() {
		semafor.metodeWait();
		return caixaDisponible;
		/*if (caixaDisponible) {
			return true;
		} else {
			return false;
		}*/
	}

	//Agafar la compra -------------------
	public void agafaCompra() {
		//Fent temps per modificar variable booleana
		for (int i = 0; i < 10000; i++) {
			//No fer res
		}

		this.caixaDisponible = false; //Canviant estat de la caixa
		System.out.println("<"+ nomCaixa +"> Llegint la compra de" 
				+ "[" + Thread.currentThread().getName() + "]");
	}

	public void cobramentCompra() {
				
		System.out.println("<"+ nomCaixa +"> Import de la compra del client " 
				+ "[" + Thread.currentThread().getName() + "] es de " + Math.round(Math.random() * 250) + " € ..");
	}

	public void ticketCompra() {
		
		System.out.println("<"+ nomCaixa +"> Donant ticket compra a " 
				+ "[" + Thread.currentThread().getName() + "] i deixant la caixa");
		this.caixaDisponible = true;
		//Alliberament condicio
		semafor.metodeWakeUp();
	}
	
	//-------------------------------------
	public synchronized void metodeWait() {
		try {
			while(!isCaixaDisponible()) {
				System.out.println("["+Thread.currentThread().getName()+"] metodeWait -> "
						+ "La caixa no esta disponible");
				wait();
			}
			
			agafaCompra();
			
		} catch (InterruptedException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	//-------------------------------------
	public synchronized void metodeCanviaEstatCaixa() {
		ticketCompra();
		System.out.println("["+Thread.currentThread().getName()+"] metodeCanviaEstatCaixa -> "
				+ "La caixa ja esta disponible");
		notifyAll();
	}
	
}
